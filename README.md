# Tinder Autolike Occasionally 1.0

Script for automatic like on Tinder Web's profiles, with specifical distribution curve: 
30% probability you will autolike someone, 68% nope, 1% boost, 1% biglike. 
Pause time ranges are a little bit higher for avoiding ban.

## Thank you

Ispired from https://github.com/jaflesch/tinder-autolike

## Warning

This code was created only for educational / fun purposes.
DO NOT TRY THIS too much times in a day.

## How to use

- Go to the tinder website
- Login
- And then depending on the website, go to https://tinder.com/app/recs or to https://www.okcupid.com/doubletake
- Open the browser's console
- Copy and paste the code from autolike.js to console
- Run the script
- You better pray for nothing happens (ie. banned from Tinder)
- Have fun! :-)

## About me

SEO and IT engineer, working as as freelance for personal web projects:
i.e 
https://trovalost.it 
https://leultime.info 
https://pagare.online
https://lipercubo.it

now working as tech SEO consultant for https://seoroma.com

